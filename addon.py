
bl_info = {
    "name" : "Marching Cubes Terrain Tool",
    "category" : "",
    "author" : "Evgeniy Dzhurov",
    "version" : (0, 1)
}

import os, sys
import bpy
from mathutils import *

mc_path = 'D:\\Dev\\compgraphics_nbu_diploma'
import sys
if mc_path not in sys.path:
    sys.path.append(mc_path)

import mc

print(mc.__file__)

class MCTerrainTextureSlot(bpy.types.PropertyGroup):
    
    texture_name = bpy.props.StringProperty(
        name = "Texture Name",
        description = "Texture Name",
        default =""
        )
        
    enabled = bpy.props.BoolProperty(
        name = "Enabled",
        description = "Enable/Disable usage ofn this texture slot",
        default = True
        )
    
    sample_frequency = bpy.props.FloatProperty(
        name = "Frequency",
        description = "Sampling frequency modifier",
        default = 1.0
        )
        
    sample_amplitude = bpy.props.FloatProperty(
        name = "Amplitude",
        description = "Sampling amplitude modifier",
        default = 1.0
        )

class MCTerrainProps(bpy.types.PropertyGroup):
    
    obj_name = bpy.props.StringProperty(
        name = "Name",
        description = "Name of the generated object",
        default = "MC_Terrain"
        )
        
    grid_cell_size = bpy.props.FloatProperty(
        name = "Grid Cell Size",
        description = "Size of a cube grid cell",
        default = 1.0
        )
        
    grid_size_x = bpy.props.IntProperty(
        name = "Grid Size X",
        description = "Number of grid cells in the X direction",
        default = 100,
        min = 1
        )
        
    grid_size_y = bpy.props.IntProperty(
        name = "Grid Size Y",
        description = "Number of grid cells in the Y direction",
        default = 100,
        min = 1
        )
        
    grid_size_z = bpy.props.IntProperty(
        name = "Grid Size Z",
        description = "Number of grid cells in the Z direction",
        default = 100,
        min = 1 
        )

    hard_floor_enable = bpy.props.BoolProperty(
        name = "Hard Floor",
        description = "Enables usage of a hard floor",
        default = False
        )
  
    hard_floor_value = bpy.props.FloatProperty(
        name = "Hard Floor",
        description = "Elevation of hard floor",
        default = 0
        )

    texture_slots = bpy.props.CollectionProperty(
        type=MCTerrainTextureSlot
        )
        
    use_custom_function = bpy.props.BoolProperty(
        name = "Custom Function Override",
        description = "Use custom surface function",
        default = False
    )
        
    custom_function_expr = bpy.props.StringProperty(
        name = "F(x,y,z)",
        description = "Define custom surface function",
        default = ""
        )
    
class MCTerrainPanel(bpy.types.Panel):
    bl_label = "MC Terrain Tool"
    bl_idname = 'mc_terrain'
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Create"
    bl_context = 'objectmode'
    bl_options = {"DEFAULT_CLOSED"}
    
    def draw(self, context):
        props = bpy.context.scene.mc_terrain_props
        layout = self.layout
        
        layout.prop(props, "obj_name")
        
        # Grid Settings
        box = layout.box()
        box.prop(props, "grid_cell_size")
        
        col = box.column(align=True)
        col.prop(props, "grid_size_x")
        col.prop(props, "grid_size_y")
        col.prop(props, "grid_size_z")
        
        # Hard Floor Settings
        box = layout.box()
        box.prop(props, "hard_floor_enable")
        box.prop(props, "hard_floor_value", text="Value")
        
        # Texture Sampling Settings
        box = layout.box()
        box.label("Textures")
        
        for item in props.texture_slots:
            item_box = box.box().split(0.9)
            
            col = item_box.column(align=True)
            col.prop_search(item, "texture_name", bpy.data, "textures", text="")
            col.separator()
            col.prop(item, "sample_frequency")
            col.prop(item, "sample_amplitude")
            
            col = item_box.column()
            col.prop(item, "enabled", icon_only=True)
        
        
        row = box.row()
        row.operator(MCTerrainAddTextureSlot.bl_idname, icon='ZOOMIN')
        row.operator(MCTerrainRemoveTextureSlot.bl_idname, icon='ZOOMOUT')
        
        # Custom Function
        box = layout.box()
        box.prop(props, "use_custom_function")
        box.prop(props, "custom_function_expr")
        
        # Generate Button
        layout.operator(MCTerrainGenerate.bl_idname, icon='MESH_ICOSPHERE')
        
class MCTerrainAddTextureSlot(bpy.types.Operator):
    bl_idname = "mesh.mc_terrain_add_tex_slot"
    bl_label = "Add"
    
    def execute(self, context):
        p = bpy.context.scene.mc_terrain_props
        p.texture_slots.add()
        return {'FINISHED'}
    
class MCTerrainRemoveTextureSlot(bpy.types.Operator):
    bl_idname = "mesh.mc_terrain_rm_tex_slot"
    bl_label = "Remove"
    
    def execute(self, context):
        p = bpy.context.scene.mc_terrain_props
        idx = len(p.texture_slots) - 1
        if idx >= 0:
            p.texture_slots.remove(idx)
        return {'FINISHED'}

class MCTerrainGenerate(bpy.types.Operator):
    """Generate mesh using marching cubes algorithm""" # Tooltip
    bl_idname = "mesh.mc_terrain_gen"
    bl_label = "Generate"

    def execute(self, context):
        p = bpy.context.scene.mc_terrain_props
        
        params = mc.Params()
        params.obj_name = p.obj_name
        params.grid_cell_size = p.grid_cell_size
        params.grid_size_x = p.grid_size_x
        params.grid_size_y = p.grid_size_y
        params.grid_size_z = p.grid_size_z
        
        if (p.use_custom_function):
            params.dens_fn = eval_custom_function(p.custom_function_expr)
        else:
            params.dens_fn = terrain_surface_func
         
        mc.generate_cpp(params)
        
        return {'FINISHED'}

def eval_custom_function(expr):
    return eval("lambda x, y, z : " + expr)

def sample_texture(tex, co):
    intensity = tex.evaluate(co).x if tex.use_color_ramp else tex.evaluate(co).w
    return 2.0 * intensity - 1.0

def saturate(x):
    return max(0, min(1, x))

def terrain_surface_func(x,y,z):
    props = bpy.context.scene.mc_terrain_props
    grid_size_x = props.grid_size_x * props.grid_cell_size
    grid_size_y = props.grid_size_y * props.grid_cell_size
    
    tex_co = Vector((x / grid_size_x, y / grid_size_y, 0))
    
    # Add plane
    result = -z
    
    # Add texture sampling
    for slot in props.texture_slots:
        texture = bpy.data.textures[slot.texture_name]
        if texture and slot.enabled:
            result = result + sample_texture(texture, tex_co * slot.sample_frequency) * slot.sample_amplitude 
    
    # Add hard floor
    if props.hard_floor_enable:
        result = result + saturate((props.hard_floor_value - z)) * 40 

    return result

def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.mc_terrain_props = bpy.props.PointerProperty(type=MCTerrainProps)

def unregister():
    del bpy.types.Scene.mc_terrain_props
    bpy.utils.unregister_module(__name__)

if __name__ == "__main__":
    register()
