import bpy, bmesh
from mathutils import *
from tables import *

mc_path = 'D:\\Dev\\compgraphics_nbu_diploma'
import sys
if mc_path not in sys.path:
    sys.path.append(mc_path)
    
print(sys.path)

class Params:
    obj_name = "MC Mesh"
    grid_cell_size = 1.0
    grid_size_x = 10
    grid_size_y = 10
    grid_size_z = 10
    dens_fn = None

class GridCell:
    verts = [None] * 8
    
    def __init__(self, i, j, k):
        self.verts[0] = (i, j, k)
        self.verts[1] = (i, j + 1, k)
        self.verts[2] = (i + 1, j + 1, k)
        self.verts[3] = (i + 1, j, k)
        self.verts[4] = (i, j, k + 1)
        self.verts[5] = (i, j + 1, k + 1)
        self.verts[6] = (i + 1, j + 1, k + 1)
        self.verts[7] = (i + 1, j, k + 1)

class Grid:
    cell_size = 1.0
    size_x = 2
    size_y = 2
    size_z = 2
    density = {}

    def get_vertex(self, ijk):
        return self.origin + Vector(ijk) * self.cell_size

    def calc_case(self, cell):
        case = 0
        for i, v in enumerate(cell.verts):
            case = case | (self.density[v] > 0) << i 
        return case

def interpolate(v1, v2, d1, d2):
    alpha = abs(d1 / (d2 - d1))
    return v1 + alpha * (v2 - v1)

def generate_primitives(bm, grid, cell):

    case = grid.calc_case(cell)
    if case == 0 or case == 255:
        return

    # Map edge index to BMVert
    edge_to_vert = [None] * 12

    for loop in tri_table[case]:
        assert(len(loop) == 3)
        for edge in loop:
            if not edge_to_vert[edge]:
                idx1 = cell.verts[edge_to_verts_table[edge][0]]
                idx2 = cell.verts[edge_to_verts_table[edge][1]]
                v1 = grid.get_vertex(idx1)
                v2 = grid.get_vertex(idx2)
                d1 = grid.density[idx1]
                d2 = grid.density[idx2]
                v3 = interpolate(v1, v2, d1, d2)
                edge_to_vert[edge] = bm.verts.new(v3)

        bm.faces.new((edge_to_vert[loop[0]], edge_to_vert[loop[1]], edge_to_vert[loop[2]]))

def evaluate_grid_points(grid, dens_fn):
    for k in range(grid.size_z + 1):
        pos_z = k * grid.cell_size
        for j in range(grid.size_y + 1):
            pos_y = j * grid.cell_size
            for i in range(grid.size_x + 1):
                pos_x = i * grid.cell_size
                v = grid.origin + Vector((pos_x, pos_y, pos_z))
                grid.density[(i, j, k)] = dens_fn(v.x, v.y, v.z)

def evaluate_cells(grid, bm):
    # Process cells
    for k in range(grid.size_z):
        for j in range(grid.size_y):
            for i in range(grid.size_x):
                cell = GridCell(i, j, k)
                generate_primitives(bm, grid, cell)

def generate(params):
    # Check params
    if params.grid_cell_size <= 0.0:
        raise Exception("Grid cell size must be > 0.0: ", params.grid_cell_size)
    if params.grid_size_x < 1:
        raise Exception("Grid size X must be >= 1: ", params.grid_size_x)
    if params.grid_size_y < 1:
        raise Exception("Grid size Y must be >= 1: ", params.grid_size_y)
    if params.grid_size_z < 1:
        raise Exception("Grid size Z must be >= 1: ", params.grid_size_z)
    if not params.dens_fn:
        raise Exception("Density function must be provided!")

    # Create grid
    grid = Grid()
    grid.size_x = params.grid_size_x
    grid.size_y = params.grid_size_y
    grid.size_z = params.grid_size_z
    grid.cell_size = params.grid_cell_size

    # Center grid at (0, 0, 0)
    grid_offset = params.grid_cell_size * -0.5
    grid.origin = Vector((params.grid_size_x, params.grid_size_y, params.grid_size_z)) * grid_offset

    # Evaluate density function
    evaluate_grid_points(grid, params.dens_fn)

    # Create geometry
    bm = bmesh.new()
    evaluate_cells(grid, bm)
    
    # Weld verts
    bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.001)
    
    # Add mesh to scene
    me = bpy.data.meshes.new("Mesh")
    bm.to_mesh(me)
    bm.free()

    scene = bpy.context.scene
    obj = bpy.data.objects.new(params.obj_name, me)
    scene.objects.link(obj)

    # Select and make active
    scene.objects.active = obj
    obj.select = True
    
    return {'FINISHED'}

def generate_cpp(params):
    from mc_utils import grid_processor
    
    # Check params
    if params.grid_cell_size <= 0.0:
        raise Exception("Grid cell size must be > 0.0: ", params.grid_cell_size)
    if params.grid_size_x < 1:
        raise Exception("Grid size X must be >= 1: ", params.grid_size_x)
    if params.grid_size_y < 1:
        raise Exception("Grid size Y must be >= 1: ", params.grid_size_y)
    if params.grid_size_z < 1:
        raise Exception("Grid size Z must be >= 1: ", params.grid_size_z)
    if not params.dens_fn:
        raise Exception("Density function must be provided!")

    # Evaluate density function
    vertexData = grid_processor(params, params.dens_fn)

    assert len(vertexData) % 9 == 0 # Float array can describe triangles: 3 * {x,y,z}

    # Create geometry
    bm = bmesh.new()
    bm_verts = []
    
    # Unpack vertex data into bmesh faces
    for i in range(0, len(vertexData), 9):
        v1 = bm.verts.new((vertexData[i], vertexData[i+1], vertexData[i+2]))
        v2 = bm.verts.new((vertexData[i+3], vertexData[i+4], vertexData[i+5]))
        v3 = bm.verts.new((vertexData[i+6], vertexData[i+7], vertexData[i+8]))
        bm.faces.new((v1, v2, v3))   
    
    # Weld verts
    bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.001)
    
    # Add mesh to scene
    me = bpy.data.meshes.new("Mesh")
    bm.to_mesh(me)
    bm.free()

    scene = bpy.context.scene
    obj = bpy.data.objects.new(params.obj_name, me)
    scene.objects.link(obj)

    # Select and make active
    scene.objects.active = obj
    obj.select = True
    
    return {'FINISHED'}


def test():
    params = Params()
    
    params.obj_name = "Main Test (Cpp module)"
    params.grid_size_x = 100
    params.grid_size_y = 100
    params.grid_size_z = 100
    params.grid_cell_size = 1.0
    params.dens_fn = lambda x, y, z: -(x*x + y*y + z*z - 15)

    generate_cpp(params)

if __name__ == "__main__":
    test()
