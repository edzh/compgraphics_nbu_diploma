
#include <iostream>
#include <vector>
#include <thread>
#include <future>
#include <chrono>
#include <cassert>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "Tables.h"

namespace py = pybind11;

////////////////////////////////////////////////////////////////////////////
struct Vert3
{
	Vert3() = default;
	Vert3(float x, float y, float z) : x(x), y(y), z(z) {}
	Vert3(int x, int y, int z) : x(float(x)), y(float(y)), z(float(z)) {}
	
	float x = 0.f, y = 0.f, z = 0.f;

	inline 
	Vert3& operator+=(const Vert3& other) noexcept { 
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	}

	inline 
	Vert3& operator-=(const Vert3& other) noexcept {
		x -= other.x;
		y -= other.y;
		z -= other.z;
		return *this;
	}

	inline 
	Vert3& operator*=(float scalar) noexcept {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return *this;
	}

	inline 
	Vert3& operator/=(float scalar) noexcept {
		x /= scalar;
		y /= scalar;
		z /= scalar;
		return *this;
	}

	inline 
	friend Vert3 operator+(const Vert3& lhs, const Vert3& rhs) noexcept {
		return Vert3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
	}

	inline 
	friend Vert3 operator-(const Vert3& lhs, const Vert3& rhs) noexcept {
		return Vert3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
	}

	inline 
	friend float operator*(const Vert3& lhs, const Vert3& rhs) noexcept {
		return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
	}

	inline 
	friend Vert3 operator*(const Vert3& v, float scalar) noexcept {
		return Vert3(v.x * scalar, v.y * scalar, v.z * scalar);
	}

	inline 
	friend Vert3 operator*(float scalar, const Vert3& v) noexcept {
		return v * scalar;
	}

	inline 
	friend Vert3 operator/(const Vert3& v, float scalar) noexcept {
		return Vert3(v.x / scalar, v.y / scalar, v.z / scalar);
	}
};
using Vertices = std::vector<Vert3>;

////////////////////////////////////////////////////////////////////////////
enum class GridDir { X, Y, Z };
using TriangleVerts = std::vector<float>;
using FutureTriangleVerts = std::future<TriangleVerts>;
using GridVolume = std::map<GridDir, std::pair<int, int>>;

////////////////////////////////////////////////////////////////////////////
struct GridPoint
{
	GridPoint() = default;
	GridPoint(int i, int j, int k) noexcept : i(i), j(j), k(k) {}

	inline
		bool operator==(const GridPoint& other) const noexcept {
		return i == other.i && j == other.j && k == other.k;
	}

	inline
		bool operator!=(const GridPoint& other) const noexcept {
		return !(*this == other);
	}

	inline
		bool operator<(const GridPoint& other) const noexcept {
		if (i != other.i) return i < other.i;
		if (j != other.j) return j < other.j;
		return k < other.k;
	}

	int i = 0, j = 0, k = 0;
};

////////////////////////////////////////////////////////////////////////////
struct Grid
{
	inline
	Vert3 MakeVert(const GridPoint& p) const noexcept {
		return origin + cell_size * Vert3(p.i, p.j, p.k);
	}

	inline
	int MakeIndex(const GridPoint& p) const noexcept {
		return p.i + (size_x + 1) * (p.j + p.k * (size_y + 1));
	}

	float Density(const GridPoint& p) const {
		return density.at(MakeIndex(p));
	}

	float& Density(const GridPoint& p) {
		return density.at(MakeIndex(p));
	}

	int					size_x = 0;
	int					size_y = 0;
	int					size_z = 0;
	Vert3				origin;
	float				cell_size = 0.f;
	py::function		dnFn;

	std::vector<float>	density;
};

////////////////////////////////////////////////////////////////////////////
struct GridChunk
{
	GridChunk(const Grid& grid) : grid(grid) {};

	std::pair<int, int> rangeX;
	std::pair<int, int> rangeY;
	std::pair<int, int> rangeZ;

	const Grid&			grid;
};

////////////////////////////////////////////////////////////////////////////
GridDir getMaxDir(int x, int y, int z)
{
	if (z > x && z > y) return GridDir::Z;
	if (y > x && y > z) return GridDir::Y;
	return GridDir::X;
}

////////////////////////////////////////////////////////////////////////////
void process_cell(int i, int j, int k, const GridChunk& chunk, TriangleVerts& result)
{
	// Make grid points for cell
	std::array<GridPoint, 8> cellPoints;
	cellPoints[0] = GridPoint(i, j, k );
	cellPoints[1] = GridPoint(i, j + 1, k);
	cellPoints[2] = GridPoint(i + 1, j + 1, k);
	cellPoints[3] = GridPoint(i + 1, j, k);
	cellPoints[4] = GridPoint(i, j, k + 1);
	cellPoints[5] = GridPoint(i, j + 1, k + 1);
	cellPoints[6] = GridPoint(i + 1, j + 1, k + 1);
	cellPoints[7] = GridPoint(i + 1, j, k + 1);

	// Calculate density and case
	std::uint8_t cellCase = 0;
	for (int i = 0; i < cellPoints.size(); ++i)
	{
		cellCase |= (chunk.grid.Density(cellPoints[i]) > 0.f) << i;
	}

	// Add triangles
	const int8_t* edgeList = GetTriangleList(cellCase);
	assert(edgeList);
	while (*edgeList >= 0)
	{
		// Create triangle vertex on edge v1, v2
		const auto& vertInd = GetEdgeVertIndices(*edgeList);
		
		const auto& p1 = cellPoints[vertInd.first];
		const auto& p2 = cellPoints[vertInd.second];

		const auto v1 = chunk.grid.MakeVert(p1);
		const auto v2 = chunk.grid.MakeVert(p2);

		const float d1 = chunk.grid.Density(p1);
		const float d2 = chunk.grid.Density(p2);

		// interpolate
		Vert3 v3 = v1 + std::abs(d1 / (d2 - d1)) * (v2 - v1);

		// Add vertex to result
		result.push_back(v3.x);
		result.push_back(v3.y);
		result.push_back(v3.z);

		++edgeList;
	}
}

////////////////////////////////////////////////////////////////////////////
TriangleVerts process_chunk(const GridChunk chunk)
{
	TriangleVerts result;

	for (int k = chunk.rangeZ.first; k < chunk.rangeZ.second; ++k)
		for (int j = chunk.rangeY.first; j < chunk.rangeY.second; ++j)
			for (int i = chunk.rangeX.first; i < chunk.rangeX.second; ++i)
				process_cell(i, j, k, chunk, result);

	return result;
}

////////////////////////////////////////////////////////////////////////////
void calculateDensity(Grid& grid)
{
	//py::print("calculateDensity:");
	//py::print();

	int numPointsX = grid.size_x + 1;
	int numPointsY = grid.size_y + 1;
	int numPointsZ = grid.size_z + 1;

	int numPoints = numPointsX * numPointsY * numPointsZ;
	grid.density.resize(numPoints);

	for (int k = 0; k < numPointsZ; ++k)
		for (int j = 0; j < numPointsY; ++j)
			for (int i = 0; i < numPointsX; ++i)
			{
				GridPoint p(i, j, k);
				Vert3 v = grid.MakeVert(p);
				grid.Density(p) = py::cast<float>(grid.dnFn(v.x, v.y, v.z));
			}
}

////////////////////////////////////////////////////////////////////////////
TriangleVerts grid_processor(py::object pyParams, py::function dnFn)
{
	const int hwThreads = std::thread::hardware_concurrency();
	//const int hwThreads = 1;

	Grid grid;
	grid.size_x = py::cast<int>(pyParams.attr("grid_size_x"));
	grid.size_y = py::cast<int>(pyParams.attr("grid_size_y"));
	grid.size_z = py::cast<int>(pyParams.attr("grid_size_z"));
	grid.cell_size = py::cast<float>(pyParams.attr("grid_cell_size"));
	grid.dnFn = dnFn;

	// Center grid at origin
	grid.origin = Vert3(grid.size_x, grid.size_y, grid.size_z) * -0.5f * grid.cell_size;

	py::print("Grid Processor");
	py::print("threads: ", hwThreads);
	py::print("grid size: ", grid.size_x, grid.size_y, grid.size_z);
	py::print("cell size: ", grid.cell_size);
	py::print("origin: ", grid.origin.x, grid.origin.y, grid.origin.z);

	// Calculate density using the provided python function
	calculateDensity(grid);

	// Init volume to include the whole grid
	GridVolume volume;
	volume[GridDir::X] = std::make_pair(0, grid.size_x);
	volume[GridDir::Y] = std::make_pair(0, grid.size_y);
	volume[GridDir::Z] = std::make_pair(0, grid.size_z);

	// Use max direction for slicing
	const auto slice_dir = getMaxDir(grid.size_x, grid.size_y, grid.size_z);
	const int slice_size = volume[slice_dir].second / hwThreads;
	const int slice_rem = volume[slice_dir].second % hwThreads;

	// Init first slice
	volume[slice_dir].second = slice_size + slice_rem;

	// Async process chunks
	std::vector<FutureTriangleVerts> results(hwThreads);
	for (int i = 0; i < hwThreads; ++i)
	{
		//py::print();
		//py::print("chunk ", i);
		//py::print("slice_x: ", volume[GridDir::X].first, volume[GridDir::X].second);
		//py::print("slice_y: ", volume[GridDir::Y].first, volume[GridDir::Y].second);
		//py::print("slice_z: ", volume[GridDir::Z].first, volume[GridDir::Z].second);

		GridChunk chunk(grid);
		chunk.rangeX = volume[GridDir::X];
		chunk.rangeY = volume[GridDir::Y];
		chunk.rangeZ = volume[GridDir::Z];

		results[i] = std::async(std::launch::async, process_chunk, chunk);
	
		// move [first, second) to next slice
		volume[slice_dir].first = volume[slice_dir].second;
		volume[slice_dir].second += slice_size;
	}

	// Combine results
	TriangleVerts result;
	for (auto& it : results)
	{
		auto verts = it.get();
		result.insert(result.end(), verts.begin(), verts.end());
	}

	return result;
}

PYBIND11_MODULE(mc_utils, m) {
	m.def("grid_processor", &grid_processor, R"pbdoc(Prints a test statement to output)pbdoc");

#ifdef VERSION_INFO
	m.attr("__version__") = VERSION_INFO;
#else
	m.attr("__version__") = "dev";
#endif
}