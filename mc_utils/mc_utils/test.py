import mc_utils as mcu

class Grid:
    size_x = 1
    size_y = 1
    size_z = 1
    cell_size = 1

grid = Grid()
fn = lambda x,y,z: -z

mcu.grid_processor(grid, fn)
