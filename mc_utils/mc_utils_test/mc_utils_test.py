import mc_utils as mcu

class Params:
    grid_size_x = 10
    grid_size_y = 10
    grid_size_z = 10
    grid_cell_size = 1

params = Params()
dnFn = lambda x,y,z: -z

result = mcu.grid_processor(params, dnFn)

assert len(result) % 9 == 0

print('\n\nResult:')
print(result)

