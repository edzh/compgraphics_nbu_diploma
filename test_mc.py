import bpy, bmesh
from mathutils import *
import mc

if __name__ == "__main__":
    cols = 16
    col_offset = 2
    row_offset = 2
    
    print("\nTest marching cube cases")
    print("=" * 30)
    
    for case in range(256):
        
        # Create unit cube
        bm = bmesh.new()
        bmesh.ops.create_cube(bm, size=1.0)
        
        # Create unit cell
        cell = mc.Cell(Vector((-0.5, -0.5, -0.5)))
        
        # Setup case and density
        cell.case = case
        for i, v in enumerate(cell.verts):
            v.d = 0.5 if (case & (1 << i)) else -0.5
            
        print(cell)
        
        # Add geometry
        mc.add_geometry(bm, cell)
        
        # Add mesh to scene
        me = bpy.data.meshes.new("Mesh")
        bm.to_mesh(me)
        bm.free()

        scene = bpy.context.scene
        obj = bpy.data.objects.new("mc_case_" + str(case), me)
        
        # Offset object position
        offset_x = (case % cols) * col_offset
        offset_y = (case // cols) * row_offset
        obj.location = Vector((offset_x + 0.5, offset_y + 0.5, 0.5))
        
        scene.objects.link(obj)    
    